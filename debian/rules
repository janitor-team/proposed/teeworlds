#!/usr/bin/make -f
# -*- makefile -*-

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

export CFLAGS := $(shell dpkg-buildflags --get CFLAGS)
export CPPFLAGS := $(shell dpkg-buildflags --get CPPFLAGS)
export LDFLAGS := $(shell dpkg-buildflags --get LDFLAGS) -Wl,--as-needed

%:
	dh $@

override_dh_auto_build:
	# Ensure we don't use embedded libs
	$(RM) -r src/engine/external/zlib
	$(RM) -r src/engine/external/pnglite
	$(RM) -r src/engine/external/wavpack
	bam -a -t -v

override_dh_installsystemd:
	dh_installsystemd --no-enable --no-start

override_dh_auto_install:
	mv build/output/teeworlds_srv build/output/teeworlds-server
	mkdir debian/icotmp
	icotool -x -o debian/icotmp other/icons/teeworlds.ico
	set -e && for size in 16 24 32 48 128 256; do \
		mkdir -p debian/teeworlds/usr/share/icons/hicolor/$${size}x$${size}/apps; \
		mv debian/icotmp/teeworlds_*_$${size}x$${size}x32.png \
		  debian/teeworlds/usr/share/icons/hicolor/$${size}x$${size}/apps/teeworlds.png; \
	done
	rmdir debian/icotmp

override_dh_auto_clean:
	-bam -c
	find -type f -name '*.o' -exec rm {} \;
	rm -rf tmp.* config.bam fake_server map_resave map_version crapnet tileset_borderfix
	rm -rf teeworlds_srv teeworlds-server dilate versionsrv packetgen mastersrv teeworlds
	rm -rf .bam debian/icotmp
	rm -f datasrc/*.pyc config.lua
	$(RM) -r build/output

override_dh_fixperms:
	dh_fixperms
ifneq (,$(filter teeworlds-data, $(shell dh_listpackages)))
	find debian/teeworlds-data -type f -exec chmod 644 {} \;
	chmod a+x debian/teeworlds-data/usr/share/games/teeworlds/teeworlds_datetime.sh
endif
