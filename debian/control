Source: teeworlds
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Jack Coulter <jscinoz@gmail.com>, Felix Geyer <fgeyer@debian.org>
Build-Depends: debhelper-compat (= 13),
               bam (>= 0.5.1),
               icoutils,
               python3,
               libfreetype6-dev,
               libgl1-mesa-dev,
               libglu1-mesa-dev,
               libjsonparser-dev,
               libpnglite-dev (>= 0.1.17-2~),
               libsdl2-dev,
               libwavpack-dev,
               libx11-dev,
               zlib1g-dev
Standards-Version: 4.5.0
Homepage: https://www.teeworlds.com/
Vcs-Git: https://salsa.debian.org/games-team/teeworlds.git
Vcs-Browser: https://salsa.debian.org/games-team/teeworlds

Package: teeworlds
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         teeworlds-data (= ${source:Version})
Description: online multi-player platform 2D shooter
 This package contains the client binary for Teeworlds.
 .
 The game features cartoon-themed graphics and physics, and relies heavily
 on classic shooter weaponry and gameplay.
 The controls are heavily inspired by the FPS genre of computer games.

Package: teeworlds-server
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         teeworlds-data (= ${source:Version})
Recommends:
 gawk
Description: server for Teeworlds; an online multi-player platform 2D shooter
 This package contains the server binary for Teeworlds.
 .
 The game features cartoon-themed graphics and physics, and relies heavily
 on classic shooter weaponry and gameplay.
 The controls are heavily inspired by the FPS genre of computer games.

Package: teeworlds-data
Architecture: all
Depends: ${misc:Depends},
         fonts-dejavu-core
Suggests: teeworlds-server, teeworlds
Description: data for Teeworlds; an online multi-player platform 2D shooter
 This package contains the static data for Teeworlds.
 .
 The game features cartoon-themed graphics and physics, and relies heavily
 on classic shooter weaponry and gameplay.
 The controls are heavily inspired by the FPS genre of computer games.
